<?php

use App\Http\Controllers\BanHangController;
use App\Http\Controllers\HoaDonBanHangController;
use App\Http\Controllers\HoaDonNhapKhoController;
use App\Http\Controllers\LoaiThucPhamController;
use App\Http\Controllers\MatHangController;
use App\Http\Controllers\NhanSuController;
use App\Http\Controllers\NhapKhoController;
use App\Http\Controllers\TestController;
use App\Models\HoaDonBanHang;
use App\Models\HoaDonNhapKho;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin/login', [NhanSuController::class, 'view']);
Route::post('/admin/login', [NhanSuController::class, 'actionLogin']);
Route::get('/admin/welcome', [TestController::class, 'view']);



Route::group(['prefix' => '/admin', 'middleware' => 'admiddleware'], function() {

    Route::get('/logout', [NhanSuController::class, 'logout']);

    Route::group(['prefix' => '/loai-thuc-pham'], function() {
        Route::get('/view', [LoaiThucPhamController::class, 'data']);
        Route::post('/load', [LoaiThucPhamController::class, 'store']);
        Route::get('/data', [LoaiThucPhamController::class, 'loadData']);
        Route::post('/update', [LoaiThucPhamController::class, 'update']);
        Route::post('/destroy', [LoaiThucPhamController::class, 'destroy']);
        Route::post('/change-status', [LoaiThucPhamController::class, 'changeStatus']);

    });
    Route::group(['prefix' => '/mat-hang'], function() {
        Route::get('/view', [MatHangController::class, 'view']);
        Route::post('/create', [MatHangController::class, 'store']);
        Route::get('/data', [MatHangController::class, 'getData']);
        Route::post('/update', [MatHangController::class, 'update']);
        Route::post('/change-status', [MatHangController::class, 'changeStatus']);
        Route::post('/destroy', [MatHangController::class, 'destroy']);


    });
    Route::group(['prefix' => '/nhap-kho'], function() {
        Route::get('/view', [NhapKhoController::class, 'index']);
        Route::post('/search', [NhapKhoController::class, 'search']);
        Route::post('/auto-complete', [NhapKhoController::class, 'autoComplete']);
        Route::get('/data', [NhapKhoController::class, 'getData']);
        Route::post('/create', [NhapKhoController::class, 'store']);
        Route::post('/update', [NhapKhoController::class, 'update']);
        Route::post('/updatePrice', [NhapKhoController::class, 'updateGia']);
        Route::post('/delete', [NhapKhoController::class, 'destroy']);
        Route::get('/lich-su', [HoaDonNhapKhoController::class, 'history']);
    });

    Route::group(['prefix' => '/hoa-don-nhap-kho'], function() {
        Route::get('/create', [HoaDonNhapKhoController::class, 'index']);
        Route::get('/detail/{id_hoa_don}', [HoaDonNhapKhoController::class, 'detail']);
        Route::get('/thong-ke', [HoaDonNhapKhoController::class, 'analytic']);
        Route::post('/thong-ke', [HoaDonNhapKhoController::class, 'analyticPost'])->name('postThongKeNhapKho');
    });

    Route::group(['prefix' => '/nhan-su'], function() {
        Route::get('/view', [NhanSuController::class, 'index']);
        Route::post('/create', [NhanSuController::class, 'store']);
        Route::get('/data', [NhanSuController::class, 'loadData']);
        Route::post('/change-status', [NhanSuController::class, 'changeStatus']);
        Route::post('/destroy', [NhanSuController::class, 'destroy']);
        Route::post('/update', [NhanSuController::class, 'update']);
        Route::post('/change-pass', [NhanSuController::class, 'changepass']);
    });

    Route::group(['prefix' => '/ban-hang'], function() {
        Route::get('/view', [BanHangController::class, 'index']);
        Route::post('/search', [NhapKhoController::class, 'search']);
        Route::post('/create', [BanHangController::class, 'store']);
        Route::get('/data', [BanHangController::class, 'getData']);
        Route::post('/update', [BanHangController::class, 'update']);
        Route::post('/updatePrice', [BanHangController::class, 'updateGia']);
        Route::post('/delete', [BanHangController::class, 'destroy']);
        Route::get('/lich-su', [BanHangController::class, 'history']);
    });


    Route::group(['prefix' => '/hoa-don-ban-hang'], function() {
        Route::get('/create', [HoaDonBanHangController::class, 'index']);
        Route::get('/detail/{id_hoa_don}', [HoaDonBanHangController::class, 'detail']);
        Route::get('/thong-ke', [HoaDonBanHangController::class, 'analytic']);
        Route::post('/thong-ke', [HoaDonBanHangController::class, 'analyticPost'])->name('postThongKeNhapKho');
    });


});

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => 'admiddleware'], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
