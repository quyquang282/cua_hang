<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a style="display: block;
            width:100%;
            text-align: center;
            margin-top: 10px;
    " href="/admin/welcome">
        <img src="/admin/dist/img/logo-svg.svg" alt="AdminLTE Logo" class="brand-image bg-white  elevation-3" style="opacity: .8;
        width: 85px;
        border-radius: 10px">
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="/admin/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="/admin/nhan-su/view" class="d-block">User</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
            <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
              <i class="fa-sharp fa-solid fa-list"></i>
              <p>
                Danh Mục
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
                <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="#" class="nav-link active">
                    <i class="fa-solid fa-cart-shopping"></i>
                    <p>Quản lí Bán Hàng
                    <i class="fa-solid right fa-angle-down"></i>
                    </p>
                    </a>
                </li>
                <li class="nav-item">
                <a href="/admin/ban-hang/view" class="nav-link">
                      <i class="fa-sharp fa-solid fa-bag-shopping"></i>
                      <p>Bán Hàng Tại Quầy</p>
                    </a>
                  </li>
                <li class="nav-item">
                    <a href="/admin/ban-hang/lich-su" class="nav-link">
                    <i class="fa-sharp fa-solid fa-clock"></i>
                    <p>Lịch Sử Bán Hàng</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/admin/hoa-don-ban-hang/thong-ke" class="nav-link">
                    <i class="fa-solid fa-chart-simple"></i>
                    <p>Thống Kê Bán Hàng</p>
                    </a>
                </li>
                </ul>
                <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="#" class="nav-link active">
                    <i class="fa-solid fa-warehouse"></i>
                    <p>Quản lí Nhập Kho
                    <i class="fa-solid right fa-angle-down"></i>
                    </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/admin/nhap-kho/view" class="nav-link">
                      <i class="fa-solid fa-warehouse"></i>
                      <p>Nhập Kho Sản Phẩm</p>
                    </a>
                  </li>
                <li class="nav-item">
                    <a href="/admin/nhap-kho/lich-su" class="nav-link">
                    <i class="fa-sharp fa-solid fa-clock"></i>
                    <p>Lịch Sử Nhập Kho</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/admin/hoa-don-nhap-kho/thong-ke" class="nav-link">
                    <i class="fa-solid fa-chart-simple"></i>
                    <p>Thống Kê Nhập Kho</p>
                    </a>
                </li>
                </ul>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="#" class="nav-link active">
                        <i class="fa-solid fa-pepper-hot"></i>
                        <p>Quản Lí Loại Thực Phẩm
                        <i class="fa-solid right fa-angle-down"></i>
                        </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/loai-thuc-pham/view" class="nav-link">
                          <i class="fa-solid fa-lemon"></i>
                          <p>Thêm Mới Loại Thực Phẩm</p>
                        </a>
                    </li>
                </ul>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="#" class="nav-link active">
                        <i class="fa-solid fa-apple-whole"></i>
                        <p>Quản Lí Mặt Hàng
                        <i class="fa-solid right fa-angle-down"></i>
                        </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/mat-hang/view" class="nav-link">
                          <i class="fa-sharp fa-solid fa-seedling"></i>
                          <p>Thêm Mới Loại Mặt Hàng</p>
                        </a>
                    </li>
                </ul>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="#" class="nav-link active">
                        <i class="fa-solid fa-user"></i>
                        <p>Quản Lí Nhân Sự
                        <i class="fa-solid right fa-angle-down"></i>
                        </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/nhan-su/view" class="nav-link">
                        <i class="fa-sharp fa-solid fa-person-walking-dashed-line-arrow-right"></i>
                        <p>Thêm Mới Nhân Sự</p>
                        </a>
                    </li>
                </ul>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="/admin/logout" class="nav-link active  ">
                            <i class="fa-solid fa-right-from-bracket"></i>
                            <p>
                                Đăng Xuất
                            </p>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
      </nav>

      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
