@extends('admin.share.master')
@section('title')
<body>
    <div class="col-md-12">
        <div class="alert alert-primary" role="alert">
            <marquee behavior="" direction="">
                <h5  behavior="" direction="">Chào Mừng Bạn Đến Với OrFarm - Mọi Sự Cố Vui Lòng Báo Admin Sớm Nhất Để Khắc Phục</h5>
            </marquee>
        </div>
    </div>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="/admin/dist/img/1.jpg" class="d-block w-100"style="height: 550px;">
            </div>
            <div class="carousel-item">
            <img src="/admin/dist/img/2.jpg" class="d-block w-100" style="height: 550px;">
            </div>
            <div class="carousel-item">
            <img src="/admin/dist/img/3.jpg" class="d-block w-100" style="height: 550px;">
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-target="#carouselExampleIndicators" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-target="#carouselExampleIndicators" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
    </div>
  </body>
@endsection
