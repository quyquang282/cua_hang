@extends('admin.share.master')
@section('title')
    Quản Lí Nhân Sự
@endsection
@section('content')
<div id="app" class="row">
    <div class="col-md-3">
        <div class="card">
            <div class="card-header">
                Thêm Mới Tài Khoản Nhân Sự
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label>Mã Nhân Sự</label>
                    <input v-model="add.ma_nhan_su" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>Họ Và Tên</label>
                    <input v-model="add.ho_va_ten"  type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input v-model="add.email" type="email" class="form-control">
                </div>
                <div class="form-group">
                    <label>Số Điện Thoại</label>
                    <input v-model="add.so_dien_thoai" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>Mật Khẩu</label>
                    <input v-model="add.password" type="password" class="form-control">
                </div>
                <div class="form-group">
                    <label>Nhập Lại Mật Khẩu</label>
                    <input v-model="add.ag_password" type="password" class="form-control">
                </div>
                <div class="form-group">
                    <label>Loại Nhân Sự</label>
                    <select v-model="add.is_master" class="form-control">
                        <option value="0">Admin</option>
                        <option value="1">Nhân Viên</option>
                    </select>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-primary" v-on:click="Them_moi_nhan_vien()">Thêm Mới Nhân Sự</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="card">
            <div class="card-header">
                Danh Sách Tài Khoản
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Họ Và Tên</th>
                            <th class="text-center">Mã Nhân Viên</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Loại Tài Khoản</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(value, key) in list_nhan_su">
                            <th class="align-middle">@{{ key + 1 }}</th>
                            <td class="align-middle">@{{ value.ho_va_ten }}</td>
                            <td class="align-middle">@{{ value.ma_nhan_su }}</td>
                            <td class="align-middle">@{{ value.email }}</td>
                            <td class="align-middle text-center text-nowrap">
                                <button v-on:click="changeOpen(value)" v-if="value.is_master == 0" class="btn btn-primary">Admin</button>
                                <button v-on:click="changeOpen(value)" v-else class="btn btn-warning">Nhân Viên</button>
                            </td>
                            <td class="align-middle text-center">
                                <button class="btn btn-info" v-on:click="click_edit(value)" data-toggle="modal" data-target="#editModal">Cập Nhật</button>
                                <button v-on:click="dele = value" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Xóa Bỏ</button>
                                <button class="btn btn-success" v-on:click="click_add(value)" data-toggle="modal" data-target="#changepass"><i class="fa-solid fa-info"></i></button>
                            </td>
                        </tr>
                    </tbody>
                    <div class="modal fade" id="changepass" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Đổi Mật Khẩu</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Đổi Mật Khẩu</label>
                                    <input v-model="changepass.password" type="password" class="form-control">
                                </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                              <button type="button" v-on:click="changePass()" class="btn btn-primary" data-dismiss="modal">Chắc Chắn</button>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-primary" role="alert">
                                    Bạn có chắc chắn muốn xóa thực phẩm: @{{ dele.ho_va_ten }}
                                </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                              <button type="button" v-on:click="xoaNhanVien()" class="btn btn-primary" data-dismiss="modal">Chắc Chắn</button>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-xl" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Mã Nhân Sự</label>
                                    <input v-model="edit.ma_nhan_su" type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Họ Và Tên</label>
                                    <input v-model="edit.ho_va_ten"  type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input v-model="edit.email" type="email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Số Điện Thoại</label>
                                    <input v-model="edit.so_dien_thoai" type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Loại Nhân Sự</label>
                                    <select v-model="edit.is_master" class="form-control">
                                        <option value="0">Admin</option>
                                        <option value="1">Nhân Viên</option>
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              <button type="button" v-on:click="cap_nhat_nhan_su()" class="btn btn-primary" data-dismiss="modal">Save changes</button>
                            </div>
                          </div>
                        </div>
                    </div>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    new Vue({
        el  :   '#app',
        data:   {
            list_nhan_su :   [],
            add:   {},
            edit:   {},
            dele: {},
            changepass: {},
            slug:'',
        },
        created() {
            this.loadData();
        },
        methods : {
            Them_moi_nhan_vien() {
                axios
                    .post('/admin/nhan-su/create', this.add)
                    .then((res) => {
                        if(res.data.status) {
                            toastr.success("Đã thêm mới thành công!");
                            this.loadData();
                        }
                    })
                    .catch((res) => {
                        var errors = res.response.data.errors;
                        $.each(errors, function(k, v) {
                            toastr.error(v[0]);
                        });
                    });
            },

            click_edit(value) {
                    this.edit = value;
                },

            click_add(value){
                this.changepass = value;
            },

            loadData() {
                axios
                    .get('/admin/nhan-su/data')
                    .then((res) => {
                       this.list_nhan_su = res.data.data;
                    })
            },

            cap_nhat_nhan_su() {
                axios
                    .post('/admin/nhan-su/update', this.edit)
                    .then((res) => {
                        if(res.data.status) {
                            toastr.success("Đã cập nhật thành công!");
                            this.loadData();
                        }
                    })
                    .catch((res) => {
                        var errors = res.response.data.errors;
                        $.each(errors, function(k, v) {
                            toastr.error(v[0]);
                        });
                    });
            },

            xoaNhanVien() {
                axios
                    .post('/admin/nhan-su/destroy', this.dele)
                    .then((res) => {
                        if(res.data.status) {
                            toastr.success("Đã xóa thành công!");
                            this.loadData();
                        }
                    })
                    .catch((res) => {
                        var errors = res.response.data.errors;
                        $.each(errors, function(k, v) {
                            toastr.error(v[0]);
                        });
                    });
            },

            changePass() {
                axios
                    .post('/admin/nhan-su/change-pass', this.changepass)
                    .then((res) => {
                        if(res.data.status) {
                            toastr.success("Đã Đổi Pass Thành Công!");
                            this.loadData();
                        }
                    })
                    .catch((res) => {
                        var errors = res.response.data.errors;
                        $.each(errors, function(k, v) {
                            toastr.error(v[0]);
                        });
                    });
            },

            changeOpen(payload) {
                axios
                    .post('/admin/nhan-su/change-status', payload)
                    .then((res) => {
                        if(res.data.status) {
                            toastr.success("Đã Thay Đổi Trạng Thái!");
                            this.loadData();
                        } else {
                            toastr.error("Thông tin không chính xác!");
                        }
                    })
                    .catch((res) => {
                        var errors = res.response.data.errors;
                        $.each(errors, function(k, v) {
                            toastr.error(v[0]);
                        });
                    });
            },

        },
    });
</script>
@endsection
