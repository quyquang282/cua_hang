@extends('admin.share.master')
@section('title')
    Quản Lý Loại Thực Phẩm
@endsection
@section('content')
    <div id="app" class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">
                    Thêm Mới Loại Thực Phẩm
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Mã Loại Thực Phẩm</label>
                        <input v-model="add.ma_thuc_pham" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Tên Loại Thực Phẩm</label>
                        <input v-model="add.ten_thuc_pham" v-on:keyup="convertToSlug(add.ten_thuc_pham)" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label >slug Loại Thực Phẩm</label>
                        <input v-model="slug" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Nguồn Gốc</label>
                        <input v-model="add.nguon_goc" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Hình Ảnh</label>
                        <div class="input-group">
                            <input id="hinh_anh" class="form-control" type="text" name="filepath">
                            <span class="input-group-prepend">
                              <a id="lfm" data-input="hinh_anh" data-preview="holder" class="btn btn-primary">
                                <i class="fa fa-picture-o"></i> Choose
                              </a>
                            </span>
                        </div>
                        <div id="holder" style="margin-top:15px;max-height:100px;"></div>
                    </div>
                    <div class="form-group">
                        <label>Trang Thái</label>
                        <select v-model="add.tinh_trang" class="form-control">
                            <option value="1">Mở Bán</option>
                            <option value="0">Tạm Ngưng</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Mô Tả</label>
                        <textarea id="mo_ta" class="form-control" cols="30" rows="5"></textarea>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-primary" v-on:click="Them_moi_loai_tp()">Thêm Mới Tin</button>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    Danh Sách Loại Sản Phẩm
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Mã Thực Phẩm</th>
                                <th class="text-center">Tên Thực Phẩm</th>
                                <th class="text-center">Nguồn Gốc</th>
                                <th class="text-center">Hình Ảnh</th>
                                <th class="text-center">Trạng Thái</th>
                                <th class="text-center">Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(value, key) in list_san_pham">
                                <th class="align-middle">@{{ key + 1 }}</th>
                                <td class="align-middle">@{{ value.ma_thuc_pham }}</td>
                                <td class="align-middle">@{{ value.ten_thuc_pham }}</td>
                                <td class="align-middle">@{{ value.nguon_goc }}</td>
                                <td class="text-center">
                                    <img v-bind:src="value.hinh_anh" class="img-thumbnail" style="height: 150px">
                                </td>
                                <td class="align-middle text-center text-nowrap">
                                    <button v-on:click="changeOpen(value)" v-if="value.tinh_trang == 1" class="btn btn-primary">Mở Bán</button>
                                    <button v-on:click="changeOpen(value)" v-else class="btn btn-warning">Tạm Ngưng</button>
                                </td>
                                <td class="align-middle text-center">
                                    <button class="btn btn-info" v-on:click="click_edit(value)" data-toggle="modal" data-target="#editModal">Cập Nhật</button>
                                    <button v-on:click="dele = value" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Xóa Bỏ</button>
                                </td>
                            </tr>
                        </tbody>
                        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-xl" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>Mã Thực Phẩm</label>
                                        <input v-model="edit.ma_thuc_pham" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Tên Thực Phẩm</label>
                                        <input v-model="edit.ten_thuc_pham" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Slug Loại Thực Phẩm</label>
                                        <input v-model="edit.slug" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Nguồn Gốc</label>
                                        <input v-model="edit.nguon_goc" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Hình Ảnh</label>
                                        <div class="input-group">
                                            <input id="edit_hinh_anh" class="form-control" type="text" name="filepath">
                                            <span class="input-group-prepend">
                                              <a id="edit_lfm" data-input="edit_hinh_anh" data-preview="edit_holder" class="btn btn-primary">
                                                <i class="fa fa-picture-o"></i> Choose
                                              </a>
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <label>Trang Thái</label>
                                            <select v-model="edit.tinh_trang" class="form-control">
                                                <option value="1">Mở Bán</option>
                                                <option value="0">Tạm Ngưng</option>
                                            </select>
                                        </div>
                                        <div id="edit_holder" style="margin-top:15px;max-height:100px;">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Mô Tả</label>
                                        <textarea id="edit_mo_ta" class="form-control" cols="30" rows="5"></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" v-on:click="cap_nhat_loai_san_pham()" class="btn btn-primary" data-dismiss="modal">Save changes</button>
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                    <div class="alert alert-primary" role="alert">
                                        Bạn có chắc chắn muốn xóa thực phẩm: @{{ dele.ten_thuc_pham }}
                                    </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                                  <button type="button" v-on:click="xoaThucPham()" class="btn btn-primary" data-dismiss="modal">Chắc Chắn</button>
                                </div>
                              </div>
                            </div>
                        </div>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        new Vue({
            el  :   '#app',
            data:   {
                list_san_pham :   [],
                add:   {},
                edit:   {},
                dele: {},
                slug:'',
            },
            created() {
                this.loadData();
            },
            methods : {
                click_edit(value) {
                    this.edit = value;
                    CKEDITOR.instances.edit_mo_ta.setData(value.mo_ta);
                    $("#edit_hinh_anh").val(value.hinh_anh);
                    $("#edit_holder").html('<img src="' + value.hinh_anh +'" style="max-height:100px;">');
                },
                Them_moi_loai_tp() {
                    this.add.hinh_anh = $("#hinh_anh").val();
                    this.add.slug = this.slug;
                    this.add.mo_ta = CKEDITOR.instances.mo_ta.getData();
                    axios
                        .post('/admin/loai-thuc-pham/load', this.add)
                        .then((res) => {
                            if(res.data.status) {
                                toastr.success("Đã thêm mới thành công!");
                                this.loadData();
                            }
                        })
                        .catch((res) => {
                            var errors = res.response.data.errors;
                            $.each(errors, function(k, v) {
                                toastr.error(v[0]);
                            });
                        });
                },

                loadData() {
                    axios
                        .get('/admin/loai-thuc-pham/data')
                        .then((res) => {
                           this.list_san_pham = res.data.data;
                        })
                },

                cap_nhat_loai_san_pham() {
                    this.edit.hinh_anh = $("#edit_hinh_anh").val();
                    this.edit.mo_ta = CKEDITOR.instances.edit_mo_ta.getData();
                    axios
                        .post('/admin/loai-thuc-pham/update', this.edit)
                        .then((res) => {
                            if(res.data.status) {
                                toastr.success("Đã cập nhật thành công!");
                                this.loadData();
                            }
                        })
                        .catch((res) => {
                            var errors = res.response.data.errors;
                            $.each(errors, function(k, v) {
                                toastr.error(v[0]);
                            });
                        });
                },

                xoaThucPham() {
                    axios
                        .post('/admin/loai-thuc-pham/destroy', this.dele)
                        .then((res) => {
                            if(res.data.status) {
                                toastr.success("Đã xóa thành công!");
                                this.loadData();
                            }
                        })
                        .catch((res) => {
                            var errors = res.response.data.errors;
                            $.each(errors, function(k, v) {
                                toastr.error(v[0]);
                            });
                        });
                },




                changeOpen(payload) {
                    axios
                        .post('/admin/loai-thuc-pham/change-status', payload)
                        .then((res) => {
                            if(res.data.status) {
                                toastr.success("Đã Thay Đổi Trạng Thái!");
                                this.loadData();
                            } else {
                                toastr.error("Thông tin không chính xác!");
                            }
                        })
                        .catch((res) => {
                            var errors = res.response.data.errors;
                            $.each(errors, function(k, v) {
                                toastr.error(v[0]);
                            });
                        });
                },

                convertToSlug(ten_thuc_pham){
                    a = this.toSlug(ten_thuc_pham);
                    this.slug = a;
                },

                toSlug(str) {
                    str = str.toLowerCase();
                    str = str
                        .normalize('NFD')
                        .replace(/[\u0300-\u036f]/g, '');
                    str = str.replace(/[đĐ]/g, 'd');
                    str = str.replace(/([^0-9a-z-\s])/g, '');
                    str = str.replace(/(\s+)/g, '-');
                    str = str.replace(/-+/g, '-');
                    str = str.replace(/^-+|-+$/g, '');

                    return str;
                },
            },
        });
    </script>
    <script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('mo_ta');
        CKEDITOR.replace('edit_mo_ta');
    </script>
    <script>
        var route_prefix = "/laravel-filemanager";
    </script>
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
        $('#lfm').filemanager('image', {prefix: route_prefix});
        $('#edit_lfm').filemanager('image', {prefix: route_prefix});

    </script>
@endsection
