<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ban_hangs', function (Blueprint $table) {
            $table->id();
            $table->integer('id_thuc_pham');
            $table->string('ten_thuc_pham');
            $table->string('ma_nhan_su');
            $table->float('so_luong_ban');
            $table->integer('don_gia_ban');
            $table->integer('id_hoa_don_ban_hang')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ban_hangs');
    }
};
