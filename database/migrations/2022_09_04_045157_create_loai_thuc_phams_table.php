<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loai_thuc_phams', function (Blueprint $table) {
            $table->id();
            $table->string('ma_thuc_pham');
            $table->string('ten_thuc_pham');
            $table->string('nguon_goc');
            $table->string('slug');
            $table->integer('tinh_trang');
            $table->text('hinh_anh');
            $table->longText('mo_ta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loai_thuc_phams');
    }
};
