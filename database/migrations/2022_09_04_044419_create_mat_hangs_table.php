<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mat_hangs', function (Blueprint $table) {
            $table->id();
            $table->string('ma_hang');
            $table->string('ten_hang');
            $table->string('ma_thuc_pham');
            $table->string('slug');
            $table->integer('tinh_trang');
            $table->float('gia_khuyen_mai');
            $table->float('don_gia_ban');
            $table->text('hinh_anh');
            $table->longText('mo_ta_san_pham');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mat_hangs');
    }
};
