<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NhanSuSeeder extends Seeder
{
    public function run()
    {
        DB::table('nhan_sus')->delete();

        DB::table('nhan_sus')->truncate();

        DB::table('nhan_sus')->insert([
            [
                'ho_va_ten'     => 'Nguyễn Quang Quy',
                'email'         => 'quyquang282@gmail.com',
                'ma_nhan_su'    =>  'NS001',
                'password'      => bcrypt('123456789'),
                'so_dien_thoai' => 1231231231,
                'is_master'     => 1,
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ],
        ]);
    }
}
