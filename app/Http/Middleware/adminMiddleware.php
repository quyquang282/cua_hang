<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class adminMiddleware
{

    public function handle(Request $request, Closure $next)
    {
        $check = Auth::guard('nhan_su')->check();
        if($check) {
            return $next($request);
        } else {
            return redirect('/admin/login');
        }
    }
}
