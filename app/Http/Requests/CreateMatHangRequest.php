<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateMatHangRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ma_hang'       => 'required|max:8|unique:mat_hangs,ma_hang',
            'ten_hang'      => 'required|min:2',
            'slug'          => 'required',
            'ma_thuc_pham'  => 'required|exists:loai_thuc_phams,id',
            'don_gia_ban'   => 'required|numeric|min:0',
            'gia_khuyen_mai'=> 'required|numeric|max:' . $this->don_gia_ban,
            'tinh_trang'    => 'required',
            'hinh_anh'      => 'required',
            'mo_ta_san_pham' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'ma_thuc_pham.required'         => 'Mã thực phẩm yêu cầu phải nhập',
            'ma_thuc_pham.max'         => 'Mã thực phẩm nhiều nhất là 8 ký tự',
            'ma_thuc_pham.unique'         => 'Mã thực phẩm đã tồn tại',
            'ten_hang.required'         => 'Tên thực phẩm yêu cầu phải nhập',
            'ten_hang.min'         => 'Mã thực phẩm ít nhất là 3 ký tự',
            'gia_khuyen_mai.*'        => 'Giá khuyến mãi yêu cầu nhập số',
            'don_gia_ban.*'        => 'Đơn giá bán yêu cầu nhập số ',
            'nguon_goc.required'         => 'Tên thực phẩm yêu cầu phải nhập',
            'slug.*'  => 'Slug yêu cầu phải nhập',
            'tinh_trang.required'         => 'Tình trạng yêu cầu phải nhập',
            'hinh_anh.required'         => 'Chưa có hình ảnh cho thực phẩm',
            'mo_ta_san_pham.required'         => 'Chưa có mô tả thực phẩm',
        ];
    }
}
