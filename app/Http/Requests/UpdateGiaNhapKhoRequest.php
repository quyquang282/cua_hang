<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateGiaNhapKhoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id'            =>  'required|exists:nhap_khos,id',
            'don_gia_nhap'  =>  'required|numeric|min:1',
        ];
    }
    public function messages()
    {
        return [
            'don_gia_nhap.*'  => 'Đơn giá yêu cầu nhập số lớn hơn 1',
        ];
    }
}
