<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NhanSuLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'email'     => 'required',
            'password'  => 'required',
            'g-recaptcha-response' => 'required|captcha',
        ];
    }

    public function messages()
    {
        return [

            'email.required'      => 'Email yêu cầu phải nhập',
            'password.required'       => 'password yêu cầu phải nhập',
        ];
    }
}
