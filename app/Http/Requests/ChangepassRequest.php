<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangepassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'password'          => 'required|min:6|max:30',
        ];
    }

    public function messages()
    {
        return [
            'password.*'       => 'password yêu cầu phải nhập trên 6 ký tự',
        ];
    }
}
