<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoaiThucPhamRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ma_thuc_pham' => 'required|max:8|unique:loai_thuc_phams,ma_thuc_pham',
            'ten_thuc_pham' => 'required|min:3|max:20|unique:loai_thuc_phams,ten_thuc_pham',
            'nguon_goc' => 'required',
            'slug'          => 'required|max:100',
            'tinh_trang' => 'required',
            'hinh_anh' => 'required',
            'mo_ta' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'ma_thuc_pham.required'         => 'Mã thực phẩm yêu cầu phải nhập',
            'ma_thuc_pham.max'         => 'Mã thực phẩm nhiều nhất là 8 ký tự',
            'ma_thuc_pham.unique'         => 'Mã thực phẩm đã tồn tại',
            'ten_thuc_pham.required'         => 'Tên thực phẩm yêu cầu phải nhập',
            'ten_thuc_pham.max'         => 'Mã thực phẩm nhiều nhất là 8 ký tự',
            'ten_thuc_pham.unique'         => 'Mã thực phẩm đã tồn tại',
            'nguon_goc.required'         => 'Tên thực phẩm yêu cầu phải nhập',
            'slug.*'  => 'Slug yêu cầu phải nhập',
            'tinh_trang.required'         => 'Tình trạng yêu cầu phải nhập',
            'hinh_anh.required'         => 'Chưa có hình ảnh cho thực phẩm',
            'mo_ta.required'         => 'Chưa có mô tả thực phẩm',
        ];
    }
}
