<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateNhanSuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'ho_va_ten'         => 'required|min:5|max:50',
            'ma_nhan_su'        => 'required|unique:nhan_sus,ma_nhan_su',
            'email'             => 'required|email|unique:nhan_sus,email',
            'password'          => 'required|min:6|max:30',
            'ag_password'       => 'required|same:password',
            'so_dien_thoai'     => 'required|digits:10|unique:nhan_sus,so_dien_thoai',
            'is_master'         => 'required|boolean',
        ];
    }

    public function messages()
    {
        return [
            'ho_va_ten.*'         => 'Họ và tên yêu cầu phải nhập từ 5 ký tự',
            'ma_nhan_su.required'     => 'Mã nhân sự yêu cầu phải nhập',
            'ma_nhan_su.unique'     => 'Mã nhân sự đã tồn tại',
            'email.required'      => 'Email yêu cầu phải nhập',
            'email.email'     => 'Email phải nhập đúng định dạng',
            'email.unique'     => 'Email đã tồn tại',
            'is_master.*'         => 'Loại nhân sự phải chọn trong option',
            'password.*'       => 'password yêu cầu phải nhập trên 6 ký tự',
            'ag_password.*'         => 'Mật khẩu nhập lại phải giống mật khẩu',
            'so_dien_thoai.required'         => 'Số điện thoại phải nhập',
            'so_dien_thoai.min'         => 'Số điện thoại phải nhập 10 số',
            'so_dien_thoai.unique'         => 'Số điện thoại đã tồn tại',
        ];
    }
}
