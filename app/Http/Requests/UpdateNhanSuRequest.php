<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateNhanSuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id'                => 'required|exists:nhan_sus,id',
            'ho_va_ten'         => 'required|min:5|max:50',
            'ma_nhan_su'        => 'required',
            'email'             => 'required|email',
            'so_dien_thoai'     => 'required|digits:10',
            'is_master'         => 'required|boolean',
        ];
    }

    public function messages()
    {
        return [
            'ho_va_ten.*'         => 'Họ và tên yêu cầu phải nhập từ 5 ký tự',
            'ma_nhan_su.required'     => 'Mã nhân sự yêu cầu phải nhập',
            'email.required'      => 'Email yêu cầu phải nhập',
            'email.email'     => 'Email phải nhập đúng định dạng',
            'so_dien_thoai.required'         => 'Số điện thoại phải nhập',
            'so_dien_thoai.min'         => 'Số điện thoại phải nhập 10 số',
            'so_dien_thoai.unique'         => 'Số điện thoại đã tồn tại',
            'is_master.*'         => 'Loại nhân sự phải chọn trong option',
        ];
    }
}
