<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBanHangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id'            =>  'required|exists:ban_hangs,id',
            'so_luong_ban' =>  'nullable|numeric',
        ];
    }

    public function messages()
    {
        return [
            'so_luong_ban.*' => 'số lượng bán bạn nhập bị sai'
        ];
    }
}
