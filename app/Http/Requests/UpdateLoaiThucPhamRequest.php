<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateLoaiThucPhamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id'        => 'required|exists:loai_thuc_phams,id',
            'ma_thuc_pham' => 'required|max:8',
            'ten_thuc_pham' => 'required',
            'slug'          => 'required|max:100',
            'nguon_goc' => 'required',
            'tinh_trang' => 'required',
            'hinh_anh' => 'required',
            'mo_ta' => 'required'
        ];

    }

    public function messages()
    {
        return [

            'ma_thuc_pham.required'         => 'Mã thực phẩm yêu cầu phải nhập',
            'ma_thuc_pham.max'         => 'Mã thực phẩm nhiều nhất là 8 ký tự',
            'ten_thuc_pham.required'         => 'Tên thực phẩm yêu cầu phải nhập',
            'ten_thuc_pham.unique'         => 'Mã thực phẩm đã tồn tại',
            'nguon_goc.required'         => 'Tên thực phẩm yêu cầu phải nhập',
            'slug.*'  => 'Slug yêu cầu phải nhập',
            'tinh_trang.required'         => 'Tình trạng yêu cầu phải nhập',
            'hinh_anh.required'         => 'Chưa có hình ảnh cho thực phẩm',
            'mo_ta.required'         => 'Chưa có mô tả thực phẩm',
        ];
    }

}
