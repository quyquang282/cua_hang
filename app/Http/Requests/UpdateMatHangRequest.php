<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMatHangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id'        => 'required|exists:mat_hangs,id',
            'ma_hang' => 'required|max:8',
            'ten_hang' => 'required|min:2',
            'slug'      => 'required',
            'ma_thuc_pham' => 'required|exists:loai_thuc_phams,id',
            'tinh_trang' => 'required',
            'don_gia_ban' => 'required|numeric|min:0',
            'gia_khuyen_mai'=> 'required|numeric|max:' . $this->don_gia_ban,
            'hinh_anh' => 'required',
            'mo_ta_san_pham' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'ma_hang.required'         => 'Mã hàng yêu cầu phải nhập',
            'ma_hang.max'         => 'Mã thực phẩm nhiều nhất là 8 ký tự',
            'ten_hang.required'         => 'Tên thực phẩm yêu cầu phải nhập',
            'ten_hang.min'         => 'Mã thực phẩm ít nhất là 3 ký tự',
            'gia_khuyen_mai.*'        => 'Giá khuyến mãi yêu cầu nhập số ',
            'don_gia_ban.*'        => 'Đơn giá bán yêu cầu nhập số lớn hơn 0',
            'nguon_goc.required'         => 'Tên thực phẩm yêu cầu phải nhập',
            'slug.*'  => 'Slug yêu cầu phải nhập',
            'tinh_trang.required'         => 'Tình trạng yêu cầu phải nhập',
            'hinh_anh.required'         => 'Chưa có hình ảnh cho thực phẩm',
            'mo_ta_san_pham.required'         => 'Chưa có mô tả thực phẩm',
        ];
    }
}
