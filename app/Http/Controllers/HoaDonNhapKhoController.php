<?php

namespace App\Http\Controllers;

use App\Models\HoaDonNhapKho;
use App\Models\mat_hang;
use App\Models\NhapKho;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class HoaDonNhapKhoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $chiTietNhapKho = NhapKho::whereNull('id_hoa_don_nhap_kho')->get(); // get, all -> array. first, find -> obj

        if(count($chiTietNhapKho) > 0) {
            $hoaDonNhapKho = HoaDonNhapKho::create([
                'hash'  => Str::uuid(),
            ]);

            $hoaDonNhapKho->ma_don_hang = 'HDNK' . (100000 + $hoaDonNhapKho->id);

            $tongTien       = 0;
            $tongSanPham    = 0;
            foreach($chiTietNhapKho as $key => $value) {
                $sanPham = mat_hang::find($value->id_thuc_pham);
                if($sanPham) {
                    $tongTien       = $tongTien + $value->so_luong_nhap * $value->don_gia_nhap;
                    $tongSanPham    = $tongSanPham + $value->so_luong_nhap;

                    $value->id_hoa_don_nhap_kho = $hoaDonNhapKho->id;
                    $value->save();
                } else {
                    $value->delete();
                }
            }

            $hoaDonNhapKho->tong_tien = $tongTien;
            $hoaDonNhapKho->tong_san_pham = $tongSanPham;
            $hoaDonNhapKho->save();

            if($tongSanPham > 0) {
                return response()->json([
                    'status'    => true,
                ]);
            } else {
                $hoaDonNhapKho->delete();

                return response()->json([
                    'status'    => false,
                    'message'   => 'Sản phẩm không tồn tại!',
                ]);
            }



        } else {
            return response()->json([
                'status'    => false,
                'message'   => 'Bạn Chưa Chọn Sản Phẩm!',
            ]);
        }
    }

    public function history()
    {
        $hoaDonNhapKho = HoaDonNhapKho::all();

        return view('admin.page.nhap_kho.lich_su', compact('hoaDonNhapKho'));
    }

    public function detail($id_hoa_don)
    {
        $chiTietNhapKho = NhapKho::where('id_hoa_don_nhap_kho', $id_hoa_don)
                                        ->join('mat_hangs', 'nhap_khos.id_thuc_pham', 'mat_hangs.id')
                                        ->select('nhap_khos.*', 'mat_hangs.ma_hang')
                                        ->get();

        if(count($chiTietNhapKho) > 0) {
            return response()->json([
                'status'    => true,
                'data'      => $chiTietNhapKho,
            ]);
        } else {
            return response()->json([
                'status'    => false,
                'message'   => 'Hóa đơn không tồn tại!',
            ]);
        }
    }

    public function viewAnalytic($begin, $end)
    {
        $data = HoaDonNhapKho::select(DB::raw('date(created_at) as date'),  DB::raw('sum(tong_tien) as total'))
                             ->whereDate('created_at', '>=', $begin)
                             ->whereDate('created_at', '<=', $end)
                             ->groupBy('date')
                             ->get();

        return $data;
    }

    public function analytic()
    {
        $end     = Carbon::now();
        $begin   = Carbon::now()->subDays(30);

        $data    = $this->viewAnalytic($begin, $end);

        $labels  = [];
        $data_js = [];
        foreach ($data as $key => $value) {
            array_push($labels,  $value->date);
            array_push($data_js, $value->total);
        }

        // dd($data->toArray(), $labels, $data_js);

        return view('admin.page.nhap_kho.thong_ke', compact('begin', 'end', 'data', 'labels', 'data_js'));
    }

    public function analyticPost(Request $request)
    {
        $end    = $request->end_date;
        $begin  = $request->from_date;

        $data    = $this->viewAnalytic($begin, $end);

        $labels  = [];
        $data_js = [];

        foreach ($data as $key => $value) {
            array_push($labels,  $value->date);
            array_push($data_js, $value->total);
        }

        return view('admin.page.nhap_kho.thong_ke', compact('begin', 'end', 'data', 'labels', 'data_js'));
    }
}
