<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoaiThucPhamRequest;
use App\Http\Requests\UpdateLoaiThucPhamRequest;
use App\Models\loai_thuc_pham;
use Illuminate\Http\Request;

class LoaiThucPhamController extends Controller
{
    public function data(){
        return view('admin.page.Loai_thuc_pham.index');
    }

    public function store(LoaiThucPhamRequest $request){

            $data = $request->all();

            loai_thuc_pham::create($data);

            return response()->json([
                'status'  => true,
            ]);
    }

    public function loadData(){

    $data = loai_thuc_pham::all();

    return response()->json([
        'data'  => $data,
    ]);
    }

    public function update(UpdateLoaiThucPhamRequest $request){
        $data = loai_thuc_pham::where('id', $request->id)->first();

        $data->update($request->all());

        return response()->json([
            'status'  => true,
        ]);
    }
    public function changeStatus(Request $request)
    {
        $news = loai_thuc_pham::find($request->id);
        if($news) {
            $news->tinh_trang = !$news->tinh_trang;
            $news->save();
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function destroy(Request $request)
    {
        loai_thuc_pham::where('id', $request->id)->first()->delete();

        return response()->json(['status' => true]);
    }

}
