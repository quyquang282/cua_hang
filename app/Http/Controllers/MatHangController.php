<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMatHangRequest;
use App\Http\Requests\UpdateMatHangRequest;
use App\Models\loai_thuc_pham;
use App\Models\mat_hang;
use Illuminate\Http\Request;

class MatHangController extends Controller
{
    public function view(){
        $matHang = loai_thuc_pham::all();
        return view('admin.page.mat_hang.index', compact('matHang'));
    }

    public function getData(){
        $data = mat_hang::join('loai_thuc_phams', 'mat_hangs.ma_thuc_pham', 'loai_thuc_phams.id')
                     ->select('mat_hangs.*', 'loai_thuc_phams.ten_thuc_pham')
                     ->get(); // Trả về array

        return response()->json([
            'data'    => $data,
        ]);


    }

    public function store(CreateMatHangRequest $request){

        $data = $request->all();

        mat_hang::create($data);

        return response()->json([
            'status'  => true,
        ]);
    }

    public function loadData(){
        $data = mat_hang::all();

        return response()->json([
            'data'  => $data,
        ]);
        dd('$data');
    }
    public function update(UpdateMatHangRequest $request){
        $data = mat_hang::where('id', $request->id)->first();

        $data->update($request->all());

        return response()->json([
            'status'  => true,
        ]);
    }
    public function destroy(Request $request)
    {
        mat_hang::where('id', $request->id)->first()->delete();

        return response()->json(['status' => true]);
    }

    public function changeStatus(Request $request)
    {
        $news = mat_hang::find($request->id);
        if($news) {
            $news->tinh_trang = !$news->tinh_trang;
            $news->save();
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

}
