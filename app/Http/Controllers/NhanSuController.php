<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangepassRequest;
use App\Http\Requests\CreateNhanSuRequest;
use App\Http\Requests\NhanSuLoginRequest;
use App\Http\Requests\UpdateNhanSuRequest;
use App\Models\NhanSu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NhanSuController extends Controller
{

    public function index()
    {

        return view('admin.page.nhan_su.index');
    }

    public function store(CreateNhanSuRequest $request)
    {
        $data               = $request->all();
        $data['password']   = bcrypt($request->password);
        NhanSu::create($data);

        return response()->json([
            'status'  => true,
        ]);


        return redirect()->back();
    }

    public function loadData(){

        $data = NhanSu::all();

        return response()->json([
            'data'  => $data,
        ]);
    }

    public function changeStatus(Request $request)
    {
        $news = NhanSu::find($request->id);
        if($news) {
            $news->is_master = !$news->is_master;
            $news->save();
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false]);
    }

    public function destroy(Request $request)
    {
        NhanSu::where('id', $request->id)->first()->delete();

        return response()->json(['status' => true]);
    }



    public function update(UpdateNhanSuRequest $request){
        $data = NhanSu::where('id', $request->id)->first();
        $data->update($request->all());

        return response()->json([
            'status'  => true,
        ]);
    }



    public function changepass(ChangepassRequest $request){
        $customer   = NhanSu::where('id', $request->id)->first();
        if($customer) {
            $customer->password   = bcrypt($request->password);
            $customer->save();

            return response()->json([
                'status'    => true,
                'message'   => 'Tài khoản của bạn đã thay đổi mật khẩu thành công!',
            ]);
        } else {
            return response()->json([
                'status'    => false,
                'message'   => 'Thông tin không tồn tại!',
            ]);
        }
    }


    public function view()
    {
        return view('admin.login');
    }


    public function actionLogin(NhanSuLoginRequest $request)
    {
        $check = Auth::guard('nhan_su')->attempt([
            'email'     => $request->email,
            'password'  => $request->password,
        ]);

        if($check) {
            toastr()->success("Đã Login Thành Công!");
            return redirect('/admin/welcome');
        } else {
            return redirect('/admin/login');
        }


    }


    public function logout()
    {
        Auth::guard('nhan_su')->logout();

        toastr()->success("Đã Logout Thành Công!");

        return redirect('/admin/login');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NhanSu  $nhanSu
     * @return \Illuminate\Http\Response
     */
}
