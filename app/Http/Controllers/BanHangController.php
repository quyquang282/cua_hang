<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBanHangRequest;
use App\Http\Requests\DeleteBanHangRequest;
use App\Http\Requests\UpdateBanHangRequest;
use App\Models\BanHang;
use App\Models\HoaDonBanHang;
use App\Models\mat_hang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BanHangController extends Controller
{

    public function index()
    {
        return view('admin.page.ban_hang.index');
    }


    public function getData()
    {
        $data = BanHang::join('mat_hangs','ban_hangs.id_thuc_pham', 'mat_hangs.id')
                              ->join('nhan_sus', 'ban_hangs.ma_nhan_su', 'nhan_sus.id')
                              ->whereNull('id_hoa_don_ban_hang')
                              ->select('ban_hangs.*', 'mat_hangs.ma_thuc_pham', 'nhan_sus.ho_va_ten')
                              ->get();


        return response()->json([
            'data' => $data,
        ]);
    }


    public function store(CreateBanHangRequest $request)
    {
        $nhan_su = Auth::guard('nhan_su')->user()->id;
        $chiTietNhapKho = BanHang::where('id_thuc_pham', $request->id_thuc_pham)
                                        ->whereNull('id_hoa_don_ban_hang')
                                        ->first();

        if($chiTietNhapKho) {
            $chiTietNhapKho->so_luong_ban++;
            $chiTietNhapKho->save();
        } else {
            $sanPham = mat_hang::find($request->id_thuc_pham);
            BanHang::create([
                'id_thuc_pham'    =>  $sanPham->id,
                'ten_thuc_pham'   =>  $sanPham->ten_hang,
                'so_luong_ban'   =>  1,
                'don_gia_ban'    =>  $sanPham->don_gia_ban,
                'ma_nhan_su'    =>  $nhan_su,
            ]);
        }

    }

    public function update(UpdateBanHangRequest $request)
    {
        $chiTietNhapKho = BanHang::where('id', $request->id)
                                        ->whereNull('id_hoa_don_ban_hang')
                                        ->first();

        if($chiTietNhapKho) {
            $chiTietNhapKho->so_luong_ban = $request->so_luong_ban;
            $chiTietNhapKho->save();
        } else {
            return response()->json([
                'status'    => false,
                'message'   => 'Sản phẩm không tồn tại!',
            ]);
        }

    }

    public function updateGia(UpdateBanHangRequest $request)
    {
        $chiTietNhapKho = BanHang::where('id', $request->id)->whereNull('id_hoa_don_ban_hang')->first();

        if($chiTietNhapKho) {
            $chiTietNhapKho->don_gia_ban = $request->don_gia_ban;
            $chiTietNhapKho->save();
        } else {
            return response()->json([
                'status'    => false,
                'message'   => 'Mặt hàng không tồn tại!',
            ]);
        }
    }
    public function destroy(DeleteBanHangRequest $request)
    {
        $chiTietNhapKho = BanHang::where('id', $request->id)
                                        ->whereNull('id_hoa_don_ban_hang')
                                        ->first();
        if($chiTietNhapKho) {
            $chiTietNhapKho->delete();
            return response()->json([
                'status' => true,
                'message' => 'Đã xóa mặt hàng!',
            ]);
        } else {
            return response()->json([
                'status'  => false,
                'message' => 'Ê, cấm chơi mất dạy nhé!',
            ]);
        }
    }

    public function history()
    {
        $hoaDonNhapKho = HoaDonBanHang::all();

        return view('admin.page.ban_hang.lich_su_ban_hang', compact('hoaDonNhapKho'));
    }


}
