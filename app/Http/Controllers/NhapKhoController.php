<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateNhapKhoRequest;
use App\Http\Requests\DeleteNhapKhoRequest;
use App\Http\Requests\UpdateGiaNhapKhoRequest;
use App\Http\Requests\UpdateNhapKhoRequest;
use App\Models\mat_hang;
use App\Models\NhanSu;
use App\Models\NhapKho;
use Illuminate\Http\Request;

class NhapKhoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.page.nhap_kho.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $search = $request->search;
        $data= mat_hang::where('ma_thuc_pham', 'like', '%'.$search.'%')
                            ->orWhere('ten_hang', 'like', '%'.$search.'%')
                            ->get();
        return response()->json([
            'data' => $data,
        ]);
    }

    public function store(CreateNhapKhoRequest $request)
    {
        $chiTietNhapKho = NhapKho::where('id_thuc_pham', $request->id_thuc_pham)
                                        ->whereNull('id_hoa_don_nhap_kho')
                                        ->first();
        if($chiTietNhapKho) {
            $chiTietNhapKho->so_luong_nhap++;
            $chiTietNhapKho->save();
        } else {
            $sanPham = mat_hang::find($request->id_thuc_pham);
            NhapKho::create([
                'id_thuc_pham'    =>  $sanPham->id,
                'ten_thuc_pham'   =>  $sanPham->ten_hang,
                'so_luong_nhap'   =>  1,
                'don_gia_nhap'    =>  $sanPham->don_gia_ban * 0.7,
            ]);
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NhapKho  $nhapKho
     * @return \Illuminate\Http\Response
     */
    public function getData()
    {
        $data = NhapKho::join('mat_hangs','nhap_khos.id_thuc_pham', 'mat_hangs.id')
                              ->whereNull('id_hoa_don_nhap_kho')
                              ->select('nhap_khos.*', 'mat_hangs.ma_thuc_pham')
                              ->get();

        return response()->json([
            'data' => $data,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NhapKho  $nhapKho
     * @return \Illuminate\Http\Response
     */
    public function updateGia(UpdateGiaNhapKhoRequest $request)
    {
        $chiTietNhapKho = NhapKho::where('id', $request->id)->whereNull('id_hoa_don_nhap_kho')->first();

        if($chiTietNhapKho) {
            $chiTietNhapKho->don_gia_nhap = $request->don_gia_nhap;
            $chiTietNhapKho->save();
        } else {
            return response()->json([
                'status'    => false,
                'message'   => 'Mặt hàng không tồn tại!',
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NhapKho  $nhapKho
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNhapKhoRequest $request)
    {
        $chiTietNhapKho = NhapKho::where('id', $request->id)
                                        ->whereNull('id_hoa_don_nhap_kho')
                                        ->first();

        if($chiTietNhapKho) {
            $chiTietNhapKho->so_luong_nhap = $request->so_luong_nhap;
            $chiTietNhapKho->save();
        } else {
            return response()->json([
                'status'    => false,
                'message'   => 'Sản phẩm không tồn tại!',
            ]);
        }

    }

    public function autoComplete(Request $request)
    {
        $data = mat_hang::select('ten_thuc_pham')
                       ->where('ten_thuc_pham', 'like', '%' . $request->ten_thuc_pham . '%')
                       ->get();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NhapKho  $nhapKho
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteNhapKhoRequest $request)
    {
        $chiTietNhapKho = NhapKho::where('id', $request->id)
                                        ->whereNull('id_hoa_don_nhap_kho')
                                        ->first();
        if($chiTietNhapKho) {
            $chiTietNhapKho->delete();
            return response()->json([
                'status' => true,
                'message' => 'Đã xóa mặt hàng!',
            ]);
        } else {
            return response()->json([
                'status'  => false,
                'message' => 'Ê, cấm chơi mất dạy nhé!',
            ]);
        }
    }
}
