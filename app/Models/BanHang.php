<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BanHang extends Model
{
    use HasFactory;
    protected $table = 'ban_hangs';

    protected $fillable = [
        'id_thuc_pham',
        'ma_nhan_su',
        'ten_thuc_pham',
        'so_luong_ban',
        'don_gia_ban',
        'id_hoa_don_ban_hang',
    ];
}
