<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class loai_thuc_pham extends Model
{
    use HasFactory;

    protected $table = 'loai_thuc_phams';

    protected $fillable =  [
    'ma_thuc_pham',
    'ten_thuc_pham',
    'nguon_goc',
    'slug',
    'tinh_trang',
    'hinh_anh',
    'mo_ta',
    ];
}
