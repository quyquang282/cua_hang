<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mat_hang extends Model
{
    use HasFactory;

    protected $table = 'mat_hangs';

    protected $fillable  = [
        'ma_hang',
        'ten_hang',
        'ma_thuc_pham',
        'slug',
        'tinh_trang',
        'gia_khuyen_mai',
        'don_gia_ban',
        'hinh_anh',
        'mo_ta_san_pham',
    ];
}
