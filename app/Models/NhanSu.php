<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class NhanSu extends Authenticatable
{
    use HasFactory;
    protected $table = 'nhan_sus';

    protected $fillable = [
        'ho_va_ten',
        'ma_nhan_su',
        'email',
        'password',
        'so_dien_thoai',
        'is_master',
        'list_rule',
    ];
}
