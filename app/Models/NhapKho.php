<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NhapKho extends Model
{
    use HasFactory;
    protected $table = 'nhap_khos';

    protected $fillable = [
        'id_thuc_pham',
        'ten_thuc_pham',
        'so_luong_nhap',
        'don_gia_nhap',
        'id_hoa_don_nhap_kho',
    ];
}
